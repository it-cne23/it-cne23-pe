# IT-CNE23-PE

## Projketmanagement

Projektmanagement ist die Anwendung von Wissen, Fähigkeiten, Tools und Techniken, um ein Projekt von Anfang bis Ende erfolgreich zu planen, zu organisieren, umzusetzen und zu kontrollieren.

Projekte sind zeitlich begrenzte Vorhaben, die spezifische Ziele, Ressourcen und Anforderungen haben. Beispiele für Projekte können die Entwicklung eines neuen Produkts, die Durchführung einer Marketingkampagne, die Errichtung eines Gebäudes oder die Einführung eines neuen Geschäftsprozesses sein.

Projektmanagement umfasst die Identifizierung von Projektzielen und Anforderungen, die Planung von Aufgaben und Meilensteinen, die Zuordnung von Ressourcen, das Risikomanagement, das Stakeholdermanagement, das Budgetmanagement, das Zeitmanagement, das Qualitätsmanagement und die Überwachung des Projektfortschritts. Ein Projektmanager ist für die Leitung und Koordination aller Aspekte eines Projekts verantwortlich, um sicherzustellen, dass das Projekt innerhalb des Zeitrahmens, des Budgets und der Qualitätserwartungen abgeschlossen wird.

## Projektmethoden

SCRUM und GANTT:
Scrum ist eine agile Projektmanagementmethode, die auf iterativer und inkrementeller Entwicklung basiert. Das Ziel von Scrum ist es, ein Produkt oder eine Dienstleistung in kurzen Zeitrahmen zu liefern, um schnell Feedback zu erhalten und Änderungen vornehmen zu können. Scrum-Teams bestehen aus einem Product Owner, einem Scrum Master und einem Team von Entwicklern.

In Scrum gibt es verschiedene Rollen, Ereignisse und Artefakte, die aufeinander abgestimmt sind, um das Projekt effektiv zu verwalten. Zu den Scrum-Ereignissen gehören Sprints, Sprint-Planung, Daily Scrum, Sprint Review und Sprint Retrospektive.

Im Gegensatz dazu ist Gantt eine Methode zur Visualisierung des Projektzeitplans in Form eines Gantt-Diagramms, das Aufgaben und Zeitrahmen darstellt. Es zeigt den Zeitplan des Projekts in Balkenform an, wobei der Beginn und das Ende der Aufgabe und ihre Dauer dargestellt werden.

Obwohl Scrum und Gantt unterschiedliche Methoden sind, können sie zusammen verwendet werden, um ein Projekt effektiv zu planen und zu verwalten. Ein Projektmanager kann beispielsweise ein Gantt-Diagramm verwenden, um den Zeitplan für das Projekt zu visualisieren und die Planung und Überwachung von Aufgaben zu erleichtern. Scrum kann dann verwendet werden, um das Team zu organisieren, die Arbeitsabläufe zu verwalten und den Fortschritt des Projekts zu verfolgen.


## Generelle Punkte beim Projektmanagement

Projektdefinition: Die Definition des Projekts, einschließlich des Umfangs, der Ziele und der Stakeholder, ist ein wichtiger erster Schritt.

Projektplanung: Die Planung des Projekts beinhaltet die Identifizierung von Aufgaben, die Zuordnung von Ressourcen und die Festlegung von Zeitrahmen.

Risikomanagement: Das Identifizieren, Bewerten und Reagieren auf Risiken im Projekt kann helfen, potenzielle Probleme zu minimieren.

Kommunikation: Die Kommunikation mit allen Stakeholdern im Projekt ist wichtig, um sicherzustellen, dass alle auf dem gleichen Stand sind.

Teammanagement: Das Management des Teams, einschließlich der Führung und Motivation der Teammitglieder, ist ein wichtiger Aspekt des Projektmanagements.

Budgetmanagement: Das Management des Projektbudgets, einschließlich der Überwachung und Kontrolle von Ausgaben, ist wichtig, um sicherzustellen, dass das Projekt im Rahmen des Budgets bleibt.

Qualitätsmanagement: Das Überwachen und Kontrollieren der Qualität im Projekt ist wichtig, um sicherzustellen, dass die Ergebnisse den Erwartungen entsprechen.

Änderungsmanagement: Das Management von Änderungen im Projekt, einschließlich der Identifizierung von Änderungen, der Bewertung von Auswirkungen und der Genehmigung von Änderungen, ist wichtig, um sicherzustellen, dass das Projekt auf Kurs bleibt.

Überwachung und Kontrolle: Die Überwachung und Kontrolle des Projektfortschritts und der Projektleistung ist wichtig, um sicherzustellen, dass das Projekt erfolgreich abgeschlossen wird.

Abschluss des Projekts: Der Abschluss des Projekts, einschließlich der Dokumentation der Ergebnisse, der Feier des Erfolgs und der Übergabe an den Kunden oder den Betrieb, ist ein wichtiger letzter Schritt im Projektmanagement.

## Projektablauf und Organisationswürfel

![Projektablauf](https://i.imgur.com/vBNQRVE.jpg)
![Organisationswürfel](https://i.imgur.com/B6bg3ib.jpg)

## Next Topic
